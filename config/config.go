package config

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

// GroupConfig spec
type GroupConfig struct {
	Name    string   `yaml:"name"`
	Admin   bool     `yaml:"admin"`
	Members []string `yaml:"members"`
}

// Customer configuration
type Customer struct {
	Name   string
	Groups []GroupConfig
}

// Config contains customer configuration
type Config struct {
	Customer Customer
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) (*Customer, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	return NewConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func NewConfig(configData string) (*Customer, error) {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer, nil
}
