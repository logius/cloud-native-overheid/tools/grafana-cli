# grafana-cli

Example:

```bash
GRAFANA_USERNAME=grafana_user
KEYCLOAK_USERNAME=keycloak_user
export GRAFANA_PASSWORD=grafana_password
export KEYCLOAK_PASSWORD=keycloak_password
REALM=keycloak_realm

CONFIG=examples/tenant.config.yaml
GRAFANA_URL=https://grafana.example.com
KEYCLOAK_URL=https://keycloak.example.com

go run main.go configure-customer --config=${CONFIG} --realm=${REALM} \
   --grafana-url=${GRAFANA_URL} --keycloak-url=${KEYCLOAK_URL} \
   --datasource=prometheus-1:http://prometheus-datasource-1.my-namespace.svc.cluster.local:9090  \
   --datasource=prometheus-2:http://prometheus-datasource-2.my-namespace.svc.cluster.local:9090
```

- The first datasource will be the default one