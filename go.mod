module gitlab.com/logius/cloud-native-overheid/tools/grafana-cli

go 1.15

require (
	github.com/Nerzal/gocloak/v8 v8.1.1
	github.com/grafana-tools/sdk v0.0.0-20210921191058-888ef9d18611
	github.com/pkg/errors v0.9.1
	github.com/sethvargo/go-password v0.2.0
	github.com/spf13/cobra v1.1.1
	gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli v0.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

//replace gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli => ../keycloak-cli
