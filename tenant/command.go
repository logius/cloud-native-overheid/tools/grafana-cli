package tenant

import (
	"context"
	"log"
	"os"

	"github.com/pkg/errors"

	"github.com/grafana-tools/sdk"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/grafana-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

type flags struct {
	config               *string
	grafanaURL           *string
	keycloakURL          *string
	oidcRealm            *string
	dataSources          *[]string
	httpMethodDatasource *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			log.Printf("Start grafana-cli")
			err := configureTenant(cmd, &flags)
			if err != nil {
				return err
			} else {
				log.Printf("Done")
			}
			return nil
		},
		Use:   "configure-customer",
		Short: "Configure Grafana tenant",
		Long:  "This command configures organization for a tenant in Grafana.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.grafanaURL = cmd.Flags().String("grafana-url", "", "Grafana URL")
	flags.keycloakURL = cmd.Flags().String("keycloak-url", "", "URL of KeyCloak")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")
	flags.dataSources = cmd.Flags().StringArray("datasource", make([]string, 0), "Datasource for Grafana")
	flags.httpMethodDatasource = cmd.Flags().String("datasource-method", "POST", "Define HTTP method used for datasources")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("grafana-url")
	cmd.MarkFlagRequired("keycloak-url")
	cmd.MarkFlagRequired("realm")

	return cmd
}

func configureTenant(cmd *cobra.Command, flags *flags) error {

	if *flags.httpMethodDatasource != "POST" && *flags.httpMethodDatasource != "GET" {
		return errors.Errorf("'%v' is not a valid method. Please use '--datasource-method=POST' or '--datasource-method=GET'.", *flags.httpMethodDatasource)
	}

	// Only get secrets from K8s cluster if not already provided
	if os.Getenv("KEYCLOAK_USERNAME") == "" || os.Getenv("KEYCLOAK_PASSWORD") == "" {
		log.Printf("KEYCLOAK_USERNAME and/or KEYCLOAK_PASSWORD empty; fetch credentials from K8s")
		username, password := k8s.GetKeycloakSecrets()
		os.Setenv("KEYCLOAK_USERNAME", username)
		os.Setenv("KEYCLOAK_PASSWORD", password)
	}

	keyCloakClient, err := keycloak.NewKeyCloakClient(*flags.keycloakURL, *flags.oidcRealm)
	if err != nil {
		return err
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	grafanaClient, err := createGrafanaClient(*flags.grafanaURL)
	if err != nil {
		return err
	}

	// Get ID for organization and switch to that organization
	organizationID, err := organization(grafanaClient, customerConfig)
	if err != nil {
		return err
	}
	statusMessage, err := grafanaClient.SwitchActualUserContext(context.TODO(), organizationID)
	if err != nil {
		log.Printf("SwitchActualUserContext statusMessage: %+v", statusMessage)
		return err
	}

	// Create Grafana users if needed
	grafanaUsers, err := users(grafanaClient, keyCloakClient, customerConfig)
	if err != nil {
		return err
	}

	// Remove membership for Grafana users that should not be there (anymore)
	_, err = deleteMembershipRemovedUsers(grafanaClient, organizationID, grafanaUsers)
	if err != nil {
		return err
	}

	// TODO: From the list: remove users from Grafana that are not in any organization anymore
	//if len(removedMembershipUsers) > 0 {
	//	if err := deleteRemovedUsers(grafanaClient, removedMembershipUsers); err != nil {
	//		return err
	//	}
	//}

	// Make users member of the customers' organization
	if err := members(grafanaClient, customerConfig, grafanaUsers, organizationID); err != nil {
		return err
	}

	if err := datasources(grafanaClient, *flags.dataSources, organizationID, *flags.httpMethodDatasource); err != nil {
		return err
	}

	return nil
}

func createGrafanaClient(grafanaURL string) (*sdk.Client, error) {
	grafanaUser := os.Getenv("GRAFANA_USERNAME")
	grafanaPassword := os.Getenv("GRAFANA_PASSWORD")

	if grafanaUser == "" {
		return nil, errors.Errorf("Missing environment variable GRAFANA_USERNAME")
	}
	if grafanaPassword == "" {
		return nil, errors.Errorf("Missing environment variable GRAFANA_PASSWORD")
	}
	return sdk.NewClient(grafanaURL, grafanaUser+":"+grafanaPassword, sdk.DefaultHTTPClient)
}
