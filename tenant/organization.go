package tenant

import (
	"context"
	"fmt"
	"log"
	"strings"

	"github.com/Nerzal/gocloak/v8"
	"github.com/grafana-tools/sdk"
	"github.com/sethvargo/go-password/password"
	"gitlab.com/logius/cloud-native-overheid/tools/grafana-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

func members(grafanaClient *sdk.Client, customerConfig *config.Customer, grafanaUsers map[string]*sdk.User, organizationID uint) error {
	log.Printf("Create members for organizationID %v if necessary ...", organizationID)
	orgUsers, err := grafanaClient.GetOrgUsers(context.TODO(), organizationID)
	if err != nil {
		return err
	}

	for _, group := range customerConfig.Groups {
		for _, member := range group.Members {
			grafanaUser := grafanaUsers[member]

			userRole := sdk.UserRole{}
			if group.Admin {
				userRole.Role = "Admin"
			} else {
				userRole.Role = "Editor"
			}

			orgMember := getOrgMember(orgUsers, grafanaUser)
			if orgMember == nil {
				userRole.LoginOrEmail = grafanaUser.Email
				log.Printf("Create organization role %q for user %q", userRole.Role, grafanaUser.Email)
				if statusMessage, err := grafanaClient.AddActualOrgUser(context.TODO(), userRole); err != nil {
					log.Printf("AddActualOrgUser statusmessage: %+v", statusMessage)
					return err
				}
			} else if orgMember.Role != userRole.Role {
				log.Printf("Update organization role %q for user %q", userRole.Role, grafanaUser.Email)
				if statusMessage, err := grafanaClient.UpdateOrgUser(context.TODO(), userRole, organizationID, grafanaUser.ID); err != nil {
					log.Printf("UpdateActualOrgUser statusmessage: %+v", statusMessage)
					return err
				}
			}
		}
	}
	return nil
}

func getOrgMember(orgUsers []sdk.OrgUser, grafanaUser *sdk.User) *sdk.OrgUser {
	for _, orgUser := range orgUsers {
		if orgUser.ID == grafanaUser.ID {
			return &orgUser
		}
	}
	return nil
}

func organization(grafanaClient *sdk.Client, customerConfig *config.Customer) (uint, error) {
	org, err := grafanaClient.GetOrgByOrgName(context.TODO(), customerConfig.Name)
	if err != nil {
		if strings.Contains(err.Error(), "404") {
			// no problem, the organization is just not there yet
		} else {
			return 0, err
		}
	}
	if org.Name == "" {
		return createOrganization(grafanaClient, customerConfig.Name)
	} else {
		return org.ID, nil
	}
}

func users(grafanaClient *sdk.Client, keyCloakClient *keycloak.Client, customerConfig *config.Customer) (map[string]*sdk.User, error) {
	log.Printf("Create users if necessary ...")
	users := make(map[string]*sdk.User, 0)

	// Loop over groups in config file, create users if needed and 'collect' users
	for _, group := range customerConfig.Groups {
		log.Printf("Group: %v", group)

		for _, member := range group.Members {
			log.Printf("Group member: %v", member)
			keycloakUser, err := keyCloakClient.GetUser(member)
			if err != nil {
				return nil, err
			}
			user, err := getOrCreateGrafanaUser(grafanaClient, keyCloakClient, keycloakUser)
			if err != nil {
				return nil, err
			}
			users[*keycloakUser.Username] = user
		}

	}
	return users, nil
}

func getKeyCloakGroupMembers(keyCloakClient *keycloak.Client, group config.GroupConfig) (map[string][]*gocloak.User, error) {
	keycloakGroupMembers := make(map[string][]*gocloak.User)

	groupID, err := keyCloakClient.GetGroupID(group.Name)
	if err != nil {
		return nil, err
	} else if groupID == "" {
		return nil, fmt.Errorf("could not find group %q in KeyCloak", group.Name)
	}

	members, err := keyCloakClient.GetGroupMembers(groupID)
	if err != nil {
		return nil, err
	}

	keycloakGroupMembers[group.Name] = getProperConfiguredUsers(members)

	return keycloakGroupMembers, nil
}

func getProperConfiguredUsers(users []*gocloak.User) []*gocloak.User {
	properUsers := make([]*gocloak.User, 0)
	for _, user := range users {
		if strings.HasPrefix(*user.Username, "_") {
			log.Printf("Skip internal user %q", *user.Username)
		} else {
			properUsers = append(properUsers, user)
		}
	}
	return properUsers
}

func getOrCreateGrafanaUser(grafanaClient *sdk.Client, keyCloakClient *keycloak.Client, keycloakUser *gocloak.User) (*sdk.User, error) {
	grafanaUser, found, err := getUser(grafanaClient, keycloakUser)
	if err != nil {
		return nil, err
	}

	//If user is found return the user
	if found {
		return grafanaUser, nil
	}

	//Make a new account
	password, err := password.Generate(20, 5, 5, false, false)
	if err != nil {
		return nil, err
	}
	grafanaUser = &sdk.User{
		Login:    *keycloakUser.Email,
		Email:    *keycloakUser.Email,
		Name:     *keycloakUser.FirstName + " " + *keycloakUser.LastName,
		Password: password,
	}
	log.Printf("Create Grafana user %q", *keycloakUser.Email)
	statusMessage, err := grafanaClient.CreateUser(context.TODO(), *grafanaUser)
	if err != nil {
		log.Printf("CreateUser statusmessage %+v", statusMessage)
		return nil, err
	}
	grafanaUser.ID = *statusMessage.ID
	return grafanaUser, nil

}

func getKeyCloakUsers(keycloakGroupMembers map[string][]*gocloak.User) map[string]*gocloak.User {
	keycloakUsers := make(map[string]*gocloak.User)
	for _, members := range keycloakGroupMembers {
		for _, member := range members {
			keycloakUsers[*member.Username] = member
		}
	}
	return keycloakUsers
}

func getUser(grafanaClient *sdk.Client, keycloakUser *gocloak.User) (*sdk.User, bool, error) {
	grafanaUsers, err := grafanaClient.SearchUsersWithPaging(context.TODO(), keycloakUser.Email, nil, nil)
	if err != nil {
		return nil, false, err
	}
	for _, grafanaUser := range grafanaUsers.Users {
		if grafanaUser.Email == *keycloakUser.Email {
			return &grafanaUser, true, nil
		}
	}
	return &sdk.User{}, false, nil
}

func createOrganization(grafanaClient *sdk.Client, organizationName string) (uint, error) {
	org := sdk.Org{
		Name: organizationName,
	}
	log.Printf("Create Grafana organization %q", organizationName)
	statusMessage, err := grafanaClient.CreateOrg(context.TODO(), org)
	if err != nil {
		log.Printf("CreateOrg statusmessage %+v", statusMessage)
		return 0, err
	}
	return *statusMessage.OrgID, nil
}

// deleteMembershipRemovedUsers deletes membership of users that are no part of the organization anymore
func deleteMembershipRemovedUsers(grafanaClient *sdk.Client, organizationId uint, validGrafanaUsersForOrganization map[string]*sdk.User) (removedMembershipUserIds []uint, err error) {

	// Make array of logins with valid users for the organization
	listOfValidUserLogins := make([]string, len(validGrafanaUsersForOrganization))
	for key := range validGrafanaUsersForOrganization {
		listOfValidUserLogins = append(listOfValidUserLogins, key)
	}

	// Get all current members of the organization
	currentOrgMembers, err := grafanaClient.GetOrgUsers(context.TODO(), organizationId)
	if err != nil {
		return nil, err
	}

	removedMembershipUserIds = make([]uint, 0)
	for _, currentOrgMember := range currentOrgMembers {
		if !userShouldBeInOrganization(listOfValidUserLogins, currentOrgMember.Login) {
			log.Printf("OrgId %v - Remove membership for user %v (%v)", organizationId, currentOrgMember.Login, currentOrgMember.ID)
			if _, err = grafanaClient.DeleteOrgUser(context.TODO(), organizationId, currentOrgMember.ID); err != nil {
				return nil, err
			}
			log.Printf("OrgId %v - Removed membership for user %v", organizationId, currentOrgMember.Login)

			removedMembershipUserIds = append(removedMembershipUserIds, currentOrgMember.ID)
		}
	}

	return removedMembershipUserIds, nil
}

func userShouldBeInOrganization(usersInConfig []string, userLoginToBeChecked string) bool {
	if userLoginToBeChecked == "admin" {
		return true
	}
	for _, userInConfig := range usersInConfig {
		if userInConfig == userLoginToBeChecked {
			return true
		}
	}
	return false
}

// TODO: remove users from grafana that are not in any organization; deleting users is not yet possible in
// the grafana-tools/sdk (https://github.com/grafana-tools/sdk).
