package tenant

import (
	"context"
	"log"
	"strings"

	"github.com/grafana-tools/sdk"
)

type jsondata struct {
	HttpMethod string `json:"httpMethod"`
}

func datasources(grafanaClient *sdk.Client, dataSources []string, organizationID uint, useHttpMethod string) error {

	for id, dataSource := range dataSources {
		// with n=1 no colons are replaced resulting in index out of range
		nameAndURL := strings.SplitN(dataSource, ":", 2)
		ds := sdk.Datasource{
			OrgID: organizationID,
			Type:  "prometheus",
			// in Grafana GUI proxy is server access, direct is browser access
			Access:    "proxy",
			Name:      nameAndURL[0],
			URL:       nameAndURL[1],
			IsDefault: id == 0, // first datasource will be default
			JSONData: jsondata{
				HttpMethod: useHttpMethod,
			},
		}

		// check if datasource already exists
		log.Printf("Find datasource by name %v", ds.Name)
		existingDatasource, err := grafanaClient.GetDatasourceByName(context.TODO(), ds.Name)
		if err != nil {
			if strings.Contains(err.Error(), "404") {
				// no problem, the datasource is just not there yet
			} else {
				return err
			}
		}
		if existingDatasource.ID != 0 {
			ds.ID = existingDatasource.ID
			log.Printf("Update datasource %v (ID: %v) for organizationID %v", ds.Name, ds.ID, ds.OrgID)
			statusMessage, err := grafanaClient.UpdateDatasource(context.TODO(), ds)
			if err != nil {
				log.Printf("UpdateDatasource statusmessage: %+v", statusMessage)
				return err
			}
		} else {
			log.Printf("Create datasource %v for organizationID %v", ds.Name, ds.OrgID)
			statusMessage, err := grafanaClient.CreateDatasource(context.TODO(), ds)
			if err != nil {
				log.Printf("CreateDatasource statusmessage: %+v", statusMessage)
				return err
			}
		}
	}
	return nil
}
